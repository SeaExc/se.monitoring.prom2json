module.exports = (obj, line) => {

  // Check if HELP is present
  const match = line.match(/^# HELP ([^\s]+) (.*)$/);

  // If not, skip
  if (!match || !match.length)
    return false;

  // Set name and help properties
  obj.name = match[1];
  obj.help = match[2];

  return true;
};