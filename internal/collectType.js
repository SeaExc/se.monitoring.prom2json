module.exports = (obj, line) => {

  // Check if type is present
  const match = line.match(/^# TYPE ([^\s]+) (.*)$/);

  // If not, skip
  if (!match || !match.length)
    return false;

  // Set type
  obj.type = match[2];

  return true;
};