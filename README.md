# Prom2Json SE
[![npm][npm-badge]][npm-url]

## Description

This small module is helping with conversion between Prometheus string output and
JSON. JSON format is not standard output for Prometheus, but some processors
prefer to use that one over any other.

Some snippets are inspired by https://github.com/anasceym/prom2json-stream

## Installation

```shell
// If you use npm
npm i prom2json-se -S

// If you use yarn
yarn add prom2json-se
```

## How to use

```javascript

// Load module
const prom2Json = require('./');

// Use module
const promString = `
...
`;
const promJson = prom2Json.convert(promString);
```

## Simple example with Prometheus client:

```javascript

const express = require('express');
const app = express();

...

const Prometheus = require('prom-client');
const prom2Json = require('prom2json-se');

// Default Metrics (every 10s)
Prometheus.collectDefaultMetrics();

// Add metrics endpoint
app.use('/metrics-json', (req, res) => {
  const json = prom2Json.convert(Prometheus.register.metrics());
  res.send(json);
});

...

app.listen(process.env.PORT || 3000);
```

## License

This module has MIT license.

[npm-badge]:https://img.shields.io/npm/v/prom2json-se.svg
[npm-url]:https://www.npmjs.com/package/prom2json-se
