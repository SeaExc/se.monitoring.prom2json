const collectHelp = require('./internal/collectHelp');
const collectType = require('./internal/collectType');
const collectValue = require('./internal/collectValue');

module.exports = ({
  convert: (text) => {
    const json = [];
    let current;
    text
      .replace('\r\n', '\n')
      .split('\n')
      .forEach(line => {
        // If this is an empty line, split
        if(!line.trim() && current) {
          json.push(current);
          current = null;
          return;
        }

        // If current object is not initialized, do it
        if(!current)
          current = { values: [] };

        // Check #HELP
        if(!current.name && collectHelp(current, line))
          return;

        // Check #TYPE
        if(!current.type && collectType(current, line))
          return;

        // Skip next if #TYPE is still not processed
        if(!current.name || !current.type) return;

        // Collect values
        collectValue(current, line);
      });

    // Return JSON
    return json;
  }
});
